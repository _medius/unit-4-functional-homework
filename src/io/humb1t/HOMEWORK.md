# Java 8 and functional programming Homework

## API and optional
1. Cache has been modified. Files are in package "cache". Method getValue() is null safe now.
2. I think it's good idea because we can check parameters usingifPresent() and handle it in functional style.

## Method references and Streams
1. Order has been modified. Classes are in package "order". Has been added method isCheaperThanHundred().
2. This method tests in main class, in method filterOrders() using method reference.

## Default methods
1. Interface and implementations are in package "factory".
Methods for creation orders with different statuses are using one createOrder() method. So we needs to implement
just this one method in a normal classes. In RandomCostOrderFactory implementation totalCost value generates
at random with specified upper bound. In SpecificCostOrderFactory implementation factory used specified totalCost value.
This functionality tests in MainForFactory class.
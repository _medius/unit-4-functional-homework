package io.humb1t.factory;

import io.humb1t.order.Order;
import io.humb1t.order.OrderStatus;

import java.util.Random;

public class RandomCostOrderFactory implements OrderFactory {

	@Override
	public Order createOrder(int costBound, OrderStatus status) {
		return new Order(new Random().nextInt(costBound), status);
	}
}

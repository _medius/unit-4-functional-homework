package io.humb1t.factory;


import io.humb1t.order.Order;
import io.humb1t.order.OrderStatus;

public class SpecificCostOrderFactory implements OrderFactory {

	@Override
	public Order createOrder(int totalCost, OrderStatus status) {
		return new Order(totalCost, status);
	}
}

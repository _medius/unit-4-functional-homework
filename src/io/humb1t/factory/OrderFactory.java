package io.humb1t.factory;


import io.humb1t.order.Order;
import io.humb1t.order.OrderStatus;

public interface OrderFactory {
	default Order createNotStartedOrder(int totalCost) {
		return createOrder(totalCost, OrderStatus.NOT_STARTED);
	}

	default Order createStartedOrder(int totalCost) {
		return createOrder(totalCost, OrderStatus.PROCESSING);
	}

	default Order createReadyOrder(int totalCost) {
		return createOrder(totalCost, OrderStatus.COMPLETED);
	}

	default Order createFailedOrder(int totalCost) {
		return createOrder(totalCost, OrderStatus.ERROR);
	}

	Order createOrder(int totalCost, OrderStatus status);
}

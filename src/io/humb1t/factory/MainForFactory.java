package io.humb1t.factory;

import io.humb1t.order.Order;

import java.util.ArrayList;
import java.util.List;

public class MainForFactory {

	private static final int COST_BOUND = 186;
	private static final int SPECIFIC_COST = 1000;

	private void testSpecificCostFactory() {
		List<Order> orders = new ArrayList<>();
		SpecificCostOrderFactory orderFactory = new SpecificCostOrderFactory();

		System.out.println("\nOrders with specific cost " + SPECIFIC_COST + ".\n");
		orders.add(orderFactory.createFailedOrder(SPECIFIC_COST));
		orders.add(orderFactory.createNotStartedOrder(SPECIFIC_COST));
		orders.add(orderFactory.createReadyOrder(SPECIFIC_COST));
		orders.add(orderFactory.createStartedOrder(SPECIFIC_COST));

		orders.forEach(System.out::println);
	}

	private void testRandomCostFactory() {
		List<Order> orders = new ArrayList<>();
		RandomCostOrderFactory orderFactory = new RandomCostOrderFactory();

		System.out.println("\nOrders with random cost which upper bound is " + COST_BOUND + ".\n");
		orders.add(orderFactory.createFailedOrder(COST_BOUND));
		orders.add(orderFactory.createNotStartedOrder(COST_BOUND));
		orders.add(orderFactory.createReadyOrder(COST_BOUND));
		orders.add(orderFactory.createStartedOrder(COST_BOUND));

		orders.forEach(System.out::println);
	}

	private void appStart() {
		testRandomCostFactory();
		testSpecificCostFactory();
	}

	public static void main(String[] args) {
		new MainForFactory().appStart();
	}
}

package io.humb1t.order;

public enum OrderStatus {

	NOT_STARTED,
	PROCESSING,
	COMPLETED,
	ERROR
}

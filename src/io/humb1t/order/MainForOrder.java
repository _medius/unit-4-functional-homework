package io.humb1t.order;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MainForOrder {

	private static final int MAX_COST = 200;
	private static final int NUMBER_OF_ORDERS = 15;

	private List<Order> orders = new ArrayList<>();

	private OrderStatus randomStatus() {
		int index = new Random().nextInt(OrderStatus.values().length);
		return OrderStatus.values()[index];
	}

	private void createData() {
		Random random = new Random(MAX_COST);

		for (int i = 0; i < NUMBER_OF_ORDERS; i++) {
			orders.add(new Order(random.nextInt(MAX_COST), randomStatus()));
		}
	}

	private void filterOrders() {
		System.out.println("Orders which total cost is smaller than 100.\n");
		orders.stream()
				.filter(Order::isCheaperThanHundred)
				.forEach(System.out::println);
	}

	private void appStart() {
		createData();

		filterOrders();
	}

	public static void main(String[] args) {
		new MainForOrder().appStart();
	}
}

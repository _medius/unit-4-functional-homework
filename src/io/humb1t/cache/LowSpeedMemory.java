package io.humb1t.cache;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class LowSpeedMemory<K, V> {

	private Map<K, V> memory = new HashMap<>();

	public void putInto(K key, V value) {
		memory.put(key, value);
	}

	public Optional<V> getFrom(K key) {
		System.out.println("Request data from low speed memory by key " + key + '.');
		return Optional.ofNullable(memory.get(key));
	}
}

package io.humb1t.cache;

import java.util.Optional;

public class MainForCache {
	private static final int DATA_BOUND = 30;
	private static final int CACHE_SIZE = 4;

	private static LowSpeedMemory<Integer, Integer> lowSpeedMemory = new LowSpeedMemory<>();
	private static Cache<Integer, Integer> cache = new Cache<>(CACHE_SIZE);

	static {
		for (int i = 0; i < DATA_BOUND; i++) {
			lowSpeedMemory.putInto(i, i);
		}
	}

	private static Integer getValue(Integer key) {
		System.out.println("\nRequest value by key " + key);

		return cache.getFrom(key).orElseGet(() -> {
			Optional<Integer> valueToReturn = lowSpeedMemory.getFrom(key);
			valueToReturn.ifPresent(value -> cache.putInto(key, value));
			return valueToReturn.get();
		});
	}

	public static void main(String[] args) {
		for (int i = 0; i < 4; i++) {
			getValue(i);
			getValue(i);
		}

		getValue(0);
		getValue(0);

		getValue(1);
		getValue(1);

		for (int i = 4; i < 7; i++) {
			getValue(i);
			getValue(i);
		}
	}
}
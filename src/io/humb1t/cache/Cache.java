package io.humb1t.cache;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;

public class Cache<K, V> {
	private int cacheSize;

	@SuppressWarnings("unchecked")
	private Map<K, V> cache = new LinkedHashMap(cacheSize, 1f, true) {

		@Override
		protected boolean removeEldestEntry(Map.Entry eldest) {
			boolean cacheOverflow = cache.size() > cacheSize;
			if (cacheOverflow) {
				System.out.println("Remove eldest element from cache. Key "
						+ eldest.getKey() + " value " + eldest.getValue());
			}
			return cacheOverflow;
		}
	};

	public Cache(int cacheSize) {
		this.cacheSize = cacheSize;
	}

	public void putInto(K key, V value) {
		System.out.println("Put data into cache with key " + key + ", value " + value + '.');
		cache.put(key, value);
	}

	public Optional <V> getFrom (K key) {
		System.out.println("Request data from cache by key " + key + '.');
		return Optional.ofNullable(cache.get(key));
	}

	public int getCacheSize() {
		return cacheSize;
	}

	public void setCacheSize(int cacheSize) {
		this.cacheSize = cacheSize;
	}


}
